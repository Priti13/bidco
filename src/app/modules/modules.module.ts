import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { ModulesRoutingModule } from './modules-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HrDetailsComponent } from './hr-details/hr-details.component';
import { PricingUpdateComponent } from './pricing-update/pricing-update.component';
import { EmployeeMasterDataComponent } from './employee-master-data/employee-master-data.component';
import { SharedServiceComponent } from './shared-service/shared-service.component';
import { PaymentInitiationComponent } from './payment-initiation/payment-initiation.component';
import { ViewEmployeeMasterDataComponent } from './view-employee-master-data/view-employee-master-data.component';
import {UpdatRPASAPComponent} from './updat-rpa-sap/updat-rpa-sap.component';
import {NgxPaginationModule} from 'ngx-pagination'; // <-- import the module
import { NgxSpinnerModule } from "ngx-spinner";
import { NavComponent } from './nav/nav.component';
import { MenuComponent } from './menu/menu.component';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import { CalenderUpdateComponent } from './calender-update/calender-update.component';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { UserDashboardComponent } from './user-dashboard/user-dashboard.component';
import { CreatePermissonComponent } from './create-permisson/create-permisson.component';
import { NotificationDetailsComponent } from './notification-details/notification-details.component';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};

@NgModule({
  declarations: [
    DashboardComponent,
    HrDetailsComponent,
    PricingUpdateComponent,
    EmployeeMasterDataComponent,
    SharedServiceComponent,
    PaymentInitiationComponent,
    ViewEmployeeMasterDataComponent,
    MenuComponent,
    NavComponent,
    CalenderUpdateComponent,
    UserDashboardComponent,
    CreatePermissonComponent,
    NotificationDetailsComponent,
    UpdatRPASAPComponent
  ],
  imports: [
    CommonModule,
    ModulesRoutingModule,
    FormsModule,
    NgxPaginationModule,
    NgxSpinnerModule,
    PerfectScrollbarModule,
    BsDatepickerModule.forRoot(),
    NgbModule
  ],
  providers: [ {
    provide: PERFECT_SCROLLBAR_CONFIG,
    useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
  }],
  exports: [
    // DashboardComponent
  ]
})
export class ModulesModule { }
