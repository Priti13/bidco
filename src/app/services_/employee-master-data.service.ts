import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../environments/environment';
import { Observable, throwError, BehaviorSubject } from 'rxjs';
import { map, tap } from 'rxjs/operators';


@Injectable({
    providedIn: 'root'
})
export class EmployeeMasterDataService {

    private viewData: BehaviorSubject<any>;

    constructor(private http: HttpClient) {
        this.viewData = new BehaviorSubject<any>(JSON.parse(localStorage.getItem('currentData')));
    }
    setData(data: any) {
        localStorage.setItem('currentData', JSON.stringify(data));
        this.viewData.next(data);
    }
    public get getData(): any {
        return this.viewData.value;
    }

    getEmployeeMasterDetails(page, pageLimit) {
        const url = environment.url + `data-tables/employee-data?skip=${page}&limit=${pageLimit}`;
        return this.http.get(url);
    }

    // get excel report
    getExcelDetails() {
        const url = environment.url + `data-tables/get-excel-report`;
        return this.http.get(url, { responseType: 'blob' as 'json' });
    }
    // serach in table
    searchDetails(formData) {
        const url = environment.url + 'data-tables/search-employee-data';
        return this.http.post(url, formData);
    }

    // update  details 
    updateEmployeeMasterDetails(formData) {
        const url = environment.url + 'data-tables/update-employee-data';
        return this.http.post(url, formData);
    }

    getNotificationCount() {
        const url = environment.url + 'data-tables/get-delta-update-count';
        return this.http.get(url);
    }

    getNotificationData(page, pageLimit) {
        const url = environment.url + `data-tables/get-delta-update-data?skip=${page}&limit=${pageLimit}`;
        return this.http.get(url);
    }
        // serach in table
        changePass(formData) {
            const url = environment.url + 'auth/change-password';
            return this.http.post(url, formData);
        }
}