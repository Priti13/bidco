import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';

import { Observable, throwError, BehaviorSubject } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';
import { environment } from './../../environments/environment';
import { User } from '../modules/user';
import { ToastrService } from 'ngx-toastr';
@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  private currentUserSubject: BehaviorSubject<any>;
  public currentUser: Observable<any>;

  constructor(private http: HttpClient, private toastr: ToastrService) {

    this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue(): User {
    return this.currentUserSubject.value;
  }

  userNewLogin(user): Observable<any> {
    const url = environment.url + 'auth/login';
    return this.http.post<any>(url, user)
      .pipe(catchError(this.errorHandler),
        map(userData => {
          if (userData['status'] === true) {
            let tempUser: any = {
              'user': userData['name'],
              'email': userData['email'],
              'role': userData['role'],
              'status':  userData['status'],
              'CDNURL':  userData['CDNURL'],
              'token': userData.bearer ? userData.bearer : undefined
            }
            localStorage.setItem('currentUser', JSON.stringify(tempUser));
            this.currentUserSubject.next(tempUser);
          }
          return userData;
        }));
  }

  logout(): Observable<any> {
    const url = environment.url + 'auth/logout';
    return this.http.post<any>(url, {})
      .pipe(catchError(this.errorHandler),
        tap(resData => {
          if (resData['status'] === true) {
            this.cleanPreviousUser()
          }
        })
      )
  }

  cleanPreviousUser() {
    localStorage.clear();
    this.currentUserSubject.next(null);
  }

  errorHandler(respError: HttpErrorResponse) {
    if (respError.error instanceof ErrorEvent) {
      this.toastr.error('Client Side Error: ' + respError);
      console.error('Client Side Error: ' + respError);
    } else {
      this.toastr.error('Server Side Error: ' + respError);
      console.error('Server Side Error: ' + respError)
    }
    return throwError(respError || 'Server Downgrade Error');
  }

}
