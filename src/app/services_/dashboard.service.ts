import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {
  
    constructor( private http: HttpClient) {
    }

    getAttendance(page, pageLimit) {
        const url = environment.url + `dashboard/get-attendance-data?skip=${page}&limit=${pageLimit}`;
        return this.http.get(url);
    }
    searchAttendance(formData, page, pageLimit) {
        const url = environment.url + `dashboard/get-attendance-data?searchValue=${formData}&skip=${page}&limit=${pageLimit}`;
        return this.http.get(url);
    }
        // get excel report
    getExcelDetailsAttendance() {
        const url = environment.url + `dashboard/get-attendance-data?exportType=Excel`;
        return this.http.get(url, { responseType: 'blob' as 'json' });
    }

    // Leave applied here
    getLeaveApplied(page, pageLimit) {
        const url = environment.url + `dashboard/get-leave-applied-data?skip=${page}&limit=${pageLimit}`;
        return this.http.get(url);
    }
    searchLeaveApplied(formData, page, pageLimit) {
        const url = environment.url + `dashboard/get-leave-applied-data?searchValue=${formData}&skip=${page}&limit=${pageLimit}`;
        return this.http.get(url);
    }
        // get excel report
    getExcelLeaveApplied() {
        const url = environment.url + `dashboard/get-leave-applied-data?exportType=Excel`;
        return this.http.get(url, { responseType: 'blob' as 'json' });
    }

    // payroll start here
    getPayrollApplied(page, pageLimit) {
        const url = environment.url + `dashboard/get-payroll-applied-data?skip=${page}&limit=${pageLimit}`;
        return this.http.get(url);
    }
    searchPayroll(formData, page, pageLimit) {
        const url = environment.url + `dashboard/get-payroll-applied-data?searchValue=${formData}&skip=${page}&limit=${pageLimit}`;
        return this.http.get(url);
    }
        // get excel report
    getExcelPayroll() {
        const url = environment.url + `dashboard/get-payroll-applied-data?exportType=Excel`;
        return this.http.get(url, { responseType: 'blob' as 'json' });
    }
    // charts
    getLeaveChartData() {
        const url = environment.url + `dashboard/get-leave-applied-data-chart`;
        return this.http.get(url);
    }
    getPayrollChartData() {
        const url = environment.url + `dashboard/get-payroll-applied-data-chart`;
        return this.http.get(url);
    }
    getAttendanceChartData() {
        const url = environment.url + `dashboard/get-attendance-data-chart`;
        return this.http.get(url);
    }
    getDateRangeChartData(day) {
        const url = environment.url + `dashboard/get-multiple-days-date-ranges-for-graph?days=${day}`;
        return this.http.get(url);
    }
    getTodayChartData() {
        const url = environment.url + `dashboard/get-today-date-ranges-for-graph`;
        return this.http.get(url);
    }
    getBotFrequencyData() {
        const url = environment.url + `dashboard/get-bot-frequency`;
        return this.http.get(url);
    }
}