import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/services_/authentication.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { DashboardService } from 'src/app/services_/dashboard.service';
import * as $ from 'jquery';
import { Chart } from 'chart.js';
import { NgxSpinnerService } from "ngx-spinner";
import * as moment from 'moment';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  // attendance
  attendanceData: any;
  pageIndexAttendance: number = 1;
  pageSizeAttendance = 10;
  totalCountAttendance: any;
  searchValueAttendance: any;
  // Leave applied
  leaveAppliedData: any;
  pageIndexLeave: number = 1;
  pageSizeLeave = 10;
  totalCountLeave: any;
  searchValueLeave: any;
  // payroll applied
  payrollAppliedData: any;
  pageIndexPayroll: number = 1;
  pageSizePayroll = 10;
  totalCountPayroll: any;
  searchValuePayroll: any;

  // charts
  piechart = [];
  botFrequency: any;
  dayWise = 30;
  CDNURL: any;
  constructor(public router: Router, private dashboardService: DashboardService, 
    private toastr: ToastrService, private spinner: NgxSpinnerService
  ) { }

  ngOnInit() {
    this.leaveChartData();
    this.payrollChartData();
    this.attendanceChartData();
    this.getBotFrequency();
    this.dateRangeChartData(this.dayWise);
    this.todayDataChartData();
  }
  // attendance start here
  attendanceClick() {
    this.searchValueAttendance = '';
    this.pageIndexAttendance = 1;
    this.totalCountAttendance = 0;
    this.getAttendanceData(this.pageIndexAttendance, this.pageSizeAttendance);
  }
  // get attendance data on load
  getAttendanceData(pageIndex, pageSize) {
    this.spinner.show();
    this.attendanceData = [];
    this.totalCountAttendance = 0;
    this.dashboardService.getAttendance(pageSize*(pageIndex-1), pageSize).subscribe(res => {
      if (res['status']) {
        this.spinner.hide();
        this.attendanceData = res['data'];
        this.totalCountAttendance = res['totalCount'];
      } else {
        this.spinner.hide();
        this.toastr.error(res['msg']);
      }
    }, err => {
      this.spinner.hide();
      this.toastr.error(err);
    })
  }
  // pagination for attendance
  pageChangedAttendance(event) {
    this.pageIndexAttendance = event;
    if(this.searchValueAttendance === ''){
      this.getAttendanceData(this.pageIndexAttendance, this.pageSizeAttendance);
    }else{
      this.getsearchInAttendance(this.searchValueAttendance);
    }
  }
  searchInAttendance(searchValue) {
    this.pageIndexAttendance = 1;
    this.getsearchInAttendance(searchValue.value);
  }
  getsearchInAttendance(searchValue){
    if(searchValue){
      this.spinner.show();
      this.attendanceData = [];
      this.totalCountAttendance = 0;
      this.dashboardService.searchAttendance(searchValue, this.pageSizeAttendance*(this.pageIndexAttendance-1), this.pageSizeAttendance).subscribe(res => {
        if (res['status']) {
          this.spinner.hide();
          this.attendanceData = res['data'];
          this.totalCountAttendance = res['totalCount'];
        } else {
          this.spinner.hide();
          this.toastr.error(res['msg']);
        }
      }, err => {
        this.spinner.hide();
        this.toastr.error(err);
      })
    }
  }
  exportToExcelAttendance() {
    this.dashboardService.getExcelDetailsAttendance().subscribe(res => {
      this.downLoadFile(res, "application/ms-excel");
    }, err => {
      this.toastr.error(err);
    })
  }
  downLoadFile(data: any, type: string) {
    let dataType = data.type;
    let binaryData = [];
    binaryData.push(data);
    let downloadLink = document.createElement('a');
    downloadLink.href = window.URL.createObjectURL(new Blob(binaryData, { type: dataType }));
    downloadLink.setAttribute('download', 'Attendance-data');
    document.body.appendChild(downloadLink);
    downloadLink.click();
  }

  // leave methods start
  leaveClick() {
    this.searchValueLeave = '';
    this.pageIndexLeave = 1;
    this.totalCountLeave = 0;
    this.getLeaveAppliedData(this.pageIndexLeave, this.pageSizeLeave);
  }
  // get Leave applied data on load
  getLeaveAppliedData(pageIndex, pageSize) {
    this.CDNURL = 'https://bidcoafrica.azureedge.net/leavefiles/';
    this.spinner.show();
    this.leaveAppliedData = [];
    this.totalCountLeave = 0;
    this.dashboardService.getLeaveApplied(pageSize*(pageIndex-1), pageSize).subscribe(res => {
      if (res['status']) {
        this.spinner.hide();
        this.leaveAppliedData = res['data'];
        this.totalCountLeave = res['totalCount'];
      } else {
        this.spinner.hide();
        this.toastr.error(res['msg']);
      }
    }, err => {
      this.spinner.hide();
      this.toastr.error(err);
    })
  }
  // pagination for leave
  pageChangedLeave(event) {
    this.pageIndexLeave = event;
    if(this.searchValueLeave === ''){
      this.getLeaveAppliedData(this.pageIndexLeave, this.pageSizeLeave);
    }else{
      this.getsearchInLeave(this.searchValueLeave);
    }
  }
  searchInLeave(searchValue) {
    this.pageIndexLeave = 1;
    this.getsearchInLeave(searchValue.value);
  }
  getsearchInLeave(searchValue){
    if(searchValue){
      this.leaveAppliedData = [];
      this.totalCountLeave = 0;
      this.spinner.show();
      this.dashboardService.searchLeaveApplied(searchValue, this.pageSizeLeave*(this.pageIndexLeave -1), this.pageSizeLeave).subscribe(res => {
        if (res['status']) {
          this.spinner.hide();
          this.leaveAppliedData = res['data'];
          this.totalCountLeave = res['totalCount'];
        } else {
          this.spinner.hide();
          this.toastr.error(res['msg']);
        }
      }, err => {
        this.spinner.hide();
        this.toastr.error(err);
      })
    }
  }
  exportToExcelLeave() {
    this.dashboardService.getExcelLeaveApplied().subscribe(res => {
      // this.toastr.error('Oops ! Something Went Wrong.');
      this.downLoadFileLeave(res, "application/ms-excel");

    }, err => {
      this.toastr.error(err);
    })
  }
  downLoadFileLeave(data: any, type: string) {
    let dataType = data.type;
    let binaryData = [];
    binaryData.push(data);
    let downloadLink = document.createElement('a');
    downloadLink.href = window.URL.createObjectURL(new Blob(binaryData, { type: dataType }));
    downloadLink.setAttribute('download', 'Leave-data');
    document.body.appendChild(downloadLink);
    downloadLink.click();
  }

  // payroll start
  payrollClick() {
    this.searchValuePayroll = '';
    this.pageIndexPayroll = 1;
    this.totalCountPayroll =0 ;
    this.getPayrollData(this.pageIndexPayroll, this.pageSizePayroll);
  }
  // get payroll data on load
  getPayrollData(pageIndex, pageSize) {
    this.spinner.show();
    this.payrollAppliedData = [];
    this.totalCountPayroll = 0;
    this.dashboardService.getPayrollApplied((pageIndex-1)*pageSize, pageSize).subscribe(res => {
      if (res['status']) {
        this.spinner.hide();
        this.payrollAppliedData = res['data'];
        this.totalCountPayroll = res['totalCount'];
      } else {
        this.spinner.hide();
        this.toastr.error(res['msg']);
      }
    }, err => {
      this.spinner.hide();
      this.toastr.error(err);
    })
  }
  // pagination
  pageChangedPayroll(event) {
    this.pageIndexPayroll = event;
    if(this.searchValuePayroll === ''){
      this.getPayrollData(this.pageIndexPayroll, this.pageSizePayroll);
    }else{
      this.getsearchInPayroll(this.searchValuePayroll);
    }
  }
  searchInPayroll(searchValue) {
    this.pageIndexPayroll = 1;
    this.getsearchInPayroll(searchValue.value);
  }
  getsearchInPayroll(searchValue){
    if(searchValue){
      this.spinner.show();
      this.payrollAppliedData = [];
      this.totalCountPayroll = 0;
      this.dashboardService.searchPayroll(searchValue, this.pageSizePayroll*(this.pageIndexPayroll-1), this.pageSizePayroll).subscribe(res => {
        if (res['status']) {
          this.spinner.hide();
          this.payrollAppliedData = res['data'];
          this.totalCountPayroll = res['totalCount'];
        } else {
          this.spinner.hide();
          this.toastr.error(res['msg']);
        }
      }, err => {
        this.spinner.hide();
        this.toastr.error(err);
      })
    }
  }
  exportToExcelPayroll() {
    this.dashboardService.getExcelPayroll().subscribe(res => {
      this.downLoadFilePayroll(res, "application/ms-excel");
    }, err => {
      this.toastr.error(err);
    })
  }

  downLoadFilePayroll(data: any, type: string) {
    let dataType = data.type;
    let binaryData = [];
    binaryData.push(data);
    let downloadLink = document.createElement('a');
    downloadLink.href = window.URL.createObjectURL(new Blob(binaryData, { type: dataType }));
    downloadLink.setAttribute('download', 'Payroll-data');
    document.body.appendChild(downloadLink);
    downloadLink.click();
  }

  leaveChartData() {
    this.spinner.show();
    this.dashboardService.getLeaveChartData().subscribe(res => {
      if (res['status']) {
        this.spinner.hide();
        const chartData = res['data'];
        this.piechart = new Chart('pieChart', {
          type: 'pie',
          data: {
            labels: Object.keys(chartData),
            datasets: [
              {
                data: Object.values(chartData),
                borderColor: '#f8f9fa',
                backgroundColor: [
                  "#ffc107",
                  "#17a2b8",
                  "#e83e8c",
                ],
                fill: true
              }
            ]
          },
        });
      } else {
        this.spinner.hide();
        this.toastr.error(res['msg']);
      }
    }, err => {
      this.spinner.hide();
      this.toastr.error(err);
    })
  }
  payrollChartData() {
    this.spinner.show();
    this.dashboardService.getPayrollChartData().subscribe(res => {
      if (res['status']) {
        this.spinner.hide();
        const chartData = res['data'];
        this.piechart = new Chart('pieChartPayroll', {
          type: 'pie',
          data: {
            labels: Object.keys(chartData),
            datasets: [
              {
                data: Object.values(chartData),
                borderColor: '#f8f9fa',
                backgroundColor: [
                  "#ffc107",
                  "#17a2b8",
                  "#e83e8c",
                  '#20c997',
                ],
                fill: true
              }
            ]
          },
        });
      } else {
        this.spinner.hide();
        this.toastr.error(res['msg']);
      }
    }, err => {
      this.spinner.hide();
      this.toastr.error(err);
    })
  }
  attendanceChartData() {
    this.spinner.show();
    this.dashboardService.getAttendanceChartData().subscribe(res => {
      if (res['status']) {
        this.spinner.hide();
        const chartData = res['data'];
        this.piechart = new Chart('pieChartAttendance', {
          type: 'pie',
          data: {
            labels: Object.keys(chartData),
            datasets: [
              {
                data: Object.values(chartData),
                borderColor: '#f8f9fa',
                backgroundColor: [
                  "#ffc107",
                  "#17a2b8",
                  "#e83e8c",
                  '#20c997',
                ],
                fill: true
              }
            ]
          },
        });
      } else {
        this.spinner.hide();
        this.toastr.error(res['msg']);
      }
    }, err => {
      this.spinner.hide();
      this.toastr.error(err);
    })
  }
  // get details data
  getBotFrequency() {
    this.spinner.show();
    this.dashboardService.getBotFrequencyData().subscribe(res => {
      this.spinner.hide();
      this.botFrequency = res['data'];
    }, err => {
      this.spinner.hide();
      this.toastr.error(err);
    })
  }
 // date range chart
  dateRangeChartData(day) {
    this.spinner.show();
    this.dashboardService.getDateRangeChartData(day).subscribe(res => {
      if (res['status']) {
        this.spinner.hide();
        const chartData = res['data'];
          this.piechart = new Chart('barChartDate', {
            type: 'bar',
            data: {
              labels: Object.values(chartData).map(x => {
                var lbl = x['date_'].substring(0, x['date_'].indexOf("T"))
                return lbl;
              }),
              datasets: [
                {
                  label: "Data",
                  backgroundColor: "#8e5ea2",
                  data: Object.values(chartData).map(x => {
                    return x['activity_count'];
                  }),
                  fill: true
                }
              ]
            },
            options: {
              title: {
                display: true,
              }
            }
          });
      } else {
        this.spinner.hide();
        this.toastr.error(res['msg']);
      }
    }, err => {
      this.spinner.hide();
      this.toastr.error(err);
    })
  }
  // today data chart
  todayDataChartData() {
    this.spinner.show();
    this.dashboardService.getTodayChartData().subscribe(res => {
      if (res['status']) {
        this.spinner.hide();
        const chartData = res['data'];
        this.piechart = new Chart('barChartToday', {
          type: 'bar',
          data: {
            labels: ["0-2", "2-4", "4-6", "6-8", "8-10", "10-12", "12-14", "14-16", "16-18", "18-20", "20-22", "22-24"],
            datasets: [
              {
                label: "Data",
                backgroundColor: "#3e95cd",
                data: Object.values(chartData).map(x => {
                  return x['activity_count'];
                }),
                fill: true
              }
            ]
          },
          options: {
            scales: {
              yAxes: [{
                ticks: {
                  beginAtZero: true
                }
              }]
            },
            title: {
              display: true,
            }
          }
        });
      } else {
        this.spinner.hide();
        this.toastr.error(res['msg']);
      }
    }, err => {
      this.spinner.hide();
      this.toastr.error(err);
    })
  }
  dayWiseChart(day){
  this.dateRangeChartData(day);
  }

  getDateFormat(date){
   const newdate = moment(date, 'YYYY-MM-DD HH:mm');
   return newdate;

  }
}
