export enum Role {
    User = 'user',
    Admin = 'super_admin',
    Hr = 'hr_user'
}