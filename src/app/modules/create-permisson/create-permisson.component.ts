import { Component, OnInit } from '@angular/core';
import { PricingService } from 'src/app/services_/pricing.service';
declare var $: any;
import { ToastrService } from 'ngx-toastr';
import { CreatePermissionService } from 'src/app/services_/create-permission.service';
import { HrDetailsService } from 'src/app/services_/hr-details.service';


@Component({
  selector: 'app-create-permisson',
  templateUrl: './create-permisson.component.html',
  styleUrls: ['./create-permisson.component.css']
})
export class CreatePermissonComponent implements OnInit {
  permissionId: {};
  permissionIdOnModal: any;

  constructor(private permissionService: CreatePermissionService,
    private hrDetailsService: HrDetailsService, private toastr: ToastrService) { }

  permissionList: any;
  persEmail: any;
  fullName: any;
  persNo: any;
  role: any
  addPermission = {
    role: '',
    persNo: '',
    email: '',
    name: ''
  };
  ngOnInit() {
    this.addPermission['role'] = 'Select Option';
    this.getPermissionListData();
  }
  // get all data on page load
  getPermissionListData() {
    this.permissionService.getcreatePermission().subscribe(res => {
      if (res['status']) {
        this.permissionList = res['data'];
      } else {
        this.toastr.error(res['msg']);
      }
    }, err => {
      this.toastr.error(err);
    })
  }

  // get all Employee Details 
  getEmployeeDetailsData(id) {
    this.hrDetailsService.getEmployeeDetails(id.value).subscribe(res => {
      if (res['status']) {
        const details = res['data'];
        this.persEmail = details[0].Email;
        this.fullName = details[0].First_Name + details[0].Last_Name;
      } else {
        this.toastr.error(res['msg']);
      }
    }, err => {
      this.toastr.error(err);
    })
  }


  // call on click of add new pricing // updateDetails_hr.First_Name+' '+updateDetails_hr.Last_Name
  addNewPermission() {
    this.addPermission = {
      role: '',
      persNo: '',
      email: '',
      name: ''
    };
    this.addPermission['role'] = 'Select Option';
  }
  // add new pricing data
  addPermissionData() {
    this.addPermission['email'] = this.persEmail;
    this.addPermission['name'] = this.fullName;
    if(this.addPermission['role'] !== 'Select Option'){
      this.permissionService.addPermission(this.addPermission).subscribe(res => {
        if (res['status']) {
          $('#modal-dialog').modal('hide');
          this.toastr.success(res['msg']);
          this.getPermissionListData();
        }else {
          this.toastr.error(res['msg']);
        }
      }, err => {
        this.toastr.error(err);
      });
    } else{
      this.toastr.error('Select Role.');
    }

  }


  // delete row data
  delete(id, email) {
    this.permissionId = {};
    this.permissionId['id'] = id;
    this.permissionIdOnModal = email;
  }
  reset(Pers_No) {
    let form = { persNo: Pers_No }
    this.permissionService.resetPermission(form).subscribe(res => {
      if (res['status']) {
        this.toastr.success(res['msg']);
      }else {
        this.toastr.error(res['msg']);
      }
    }, err => {
      this.toastr.error(err);
    });
  }
  confirm() {
    $('#exampleModal').modal('hide');
    this.permissionService.deletePermission(this.permissionId).subscribe(res => {
      if (res['status']) {
        this.toastr.success(res['msg']);
        this.getPermissionListData();
      }else {
        this.toastr.error(res['msg']);
      }
    }, err => {
      this.toastr.error(err);
    });
  }
  decline() {
  }

  updatePermission() {
    $('#modal-dialog').modal('hide');
  }


}
