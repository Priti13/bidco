import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/services_/authentication.service';
import { EmployeeMasterDataService } from 'src/app/services_/employee-master-data.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { User } from '../user';
declare var $: any;

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {
  public show:boolean = false;
  currentUser: User;
  name: string;
  passwordForm: {currPass: string, newPass: string, reNewPass: string};
  public noOfNotification: number = 0;
  showN: boolean;
  constructor(private authenticationService: AuthenticationService,
    public router: Router,
    private toastr: ToastrService,
    private employeeService: EmployeeMasterDataService) { }

  ngOnInit() {
    //   const displayName = localStorage.getItem('displayName');
    //   if(displayName){
    // this.userName = displayName;
    //   }
    this.passwordForm = {currPass: '', newPass: '', reNewPass: ''};
    this.authenticationService.currentUser.subscribe(x => this.currentUser = x);
    this.name = this.currentUser.user;
    this.employeeService.getNotificationCount().subscribe(resData => this.noOfNotification = resData['total'])
  }
  logout() {
    this.authenticationService.logout().subscribe(
      resData => {
        if (resData['status'] === true) {
          localStorage.removeItem('currentUser');
          this.router.navigate(['/login'])
        } else {
          this.toastr.error(resData['msg']);
        }
      }
    );
  }

  changePassword(){
    if(this.passwordForm['newPass'] === this.passwordForm['reNewPass']){
      this.employeeService.changePass(this.passwordForm).subscribe(res => {
        this.toastr.success(res['msg']);
        $('#modal-changePass').modal('hide');
        this.passwordForm = {currPass: '', newPass: '', reNewPass: ''};
      }, err => {
        this.toastr.error(err);
      })
    } else{
      this.toastr.error('New password and Confirm password should be same.');
    }
  }

  closeToggle(){
    this.show = false;
    this.showN = false;
  }
  toggle() {
    this.show = !this.show;
    this.showN = false;
   }

   toggleNotification() {
    this.showN = !this.showN;
    this.show = false;
   }

}
