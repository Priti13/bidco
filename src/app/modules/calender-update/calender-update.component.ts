import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
declare var $: any;
import { NgxSpinnerService } from 'ngx-spinner';
import { CalenderUpdateService } from 'src/app/services_/calender-update.service';

const now = new Date();
interface MyForm {}
@Component({
  selector: 'app-calender-update',
  templateUrl: './calender-update.component.html',
  styleUrls: ['./calender-update.component.css']
})
export class CalenderUpdateComponent implements OnInit {

  calenderData: any;
  currentYear: any;
  allYearValue: any = [];
  selectedCountry: any;
  selectedYear: any;
  // for delete
  holidayId: {};
  holidayName: any;

  // update
  updateForm = {};
  editDate: any;
  minDateUpdate = {year: now.getFullYear()-1, month: now.getMonth()+1, date: now.getDate()};
  countrycodeDetails: any
  maxDateUpdate ={year: now.getFullYear()+3, month: now.getMonth()+1, date: now.getDate()};
  holidyName: string;

  // add
  addDate: any;
  minDateAdd = {year: now.getFullYear(), month: now.getMonth()+1, date: now.getDate()};
  maxDateAdd =  {year: now.getFullYear()+4, month: now.getMonth()+1, date: now.getDate()};
  allYearValueAdd: any = [];
  selectedCountryAdd: any;
  selectedYearAdd: any;
  holidayForm: any[] = [];


  constructor(public router: Router, private calenderService: CalenderUpdateService,
    private toastr: ToastrService, private spinner: NgxSpinnerService) { }

  ngOnInit() {
    this.selectedCountry = 'BAL';
    this.selectedCountryAdd = 'BAL';

    this.dateInitialization();
    this.holidayFormInitialization();
    this.countryDetails();
    this.calenderDetails(this.selectedYear, this.selectedCountry);
  // this.countrycodeDetails = ['ind' , 'bal', 'ing', 'iwej', 'ong', 'hds'];
  }

  // date picker initialization in edit and add
  dateInitialization() {
    this.currentYear = new Date().getFullYear();
    this.selectedYear = this.currentYear;
    this.selectedYearAdd = this.currentYear;
    this.allYearValue[0] = this.currentYear - 1;
    this.allYearValueAdd[0] = this.currentYear;
    let j = 0;
    for (let i = 1; i <= 4; i++) {
      this.allYearValue[i] = this.currentYear + j;
      this.allYearValueAdd[i] = this.currentYear + j + 1;
      j++;
    }
    // update
    // this.minDateUpdate = {year: now.getFullYear()-1, month: now.getMonth()+1, date: now.getDate()};
    // this.maxDateUpdate = {year: now.getFullYear()+3, month: now.getMonth()+1, date: now.getDate()};
    // this.minDateUpdate.setFullYear(this.minDateUpdate.getFullYear() - 1);
    // this.maxDateUpdate.setFullYear(this.maxDateUpdate.getFullYear() + 3);
    // add
    // this.minDateAdd = {year: now.getFullYear(), month: now.getMonth()+1, date: now.getDate()};
    // this.maxDateAdd = {year: now.getFullYear()+4, month: now.getMonth()+1, date: now.getDate()};

  }

  holidayFormInitialization(){
    const holdyForm = {
      date: {},
      holiday: ''
    };
    this.holidayForm.push(holdyForm);
  }

 // add new row in array
 addHolidayForm(){
  const holdyForm = {
    date: {},
    holiday: ''
  };
  this.holidayForm.push(holdyForm);
 }
 // get country code
 countryDetails() {
  this.calenderService.companycode().subscribe(res => {
    if (res['status']) {
    this.countrycodeDetails = res['data'];
    }
  }, err => {

  })
}

  calenderDetails(year, country) {
    this.spinner.show();
    this.calenderData = [];
    this.calenderService.getCalenderDetails(year, country).subscribe(res => {
      if (res['status']) {
        this.spinner.hide();
        // this.calenderData = res['data'];
        this.calenderData = Object.values(res['data']).map(x => {
          x['Date'] = x['Date'].substring(0, x['Date'].indexOf("T"));
          return x;
        })
      } else {
        this.spinner.hide();
        this.toastr.error(res['msg']);
      }
    }, err => {
      this.spinner.hide();
      this.toastr.error(err);
    })
  }
  // on year select
  onYearSelected(value: string) {
    this.selectedYear = value
    this.calenderDetails(this.selectedYear, this.selectedCountry);
  }
  // on country select
  onCountrySelected(value: string) {
    this.selectedCountry = value
    this.calenderDetails(this.selectedYear, this.selectedCountry);
  }

  // add holiday start here
  onYearSelectedAdd(value: string) { // select year in add
    this.selectedYearAdd = value
  }
  // on country select in add
  onCountrySelectedAdd(value: string) {
    this.selectedCountryAdd = value
  }

  addHoliday(){

 for(let i= 0; i < this.holidayForm.length; i++){
   const newDate  = this.holidayForm[i].date;
   this.holidayForm[i].date = newDate.year + '-' + newDate.month + '-'+ newDate.day;
 }
 const newDetails = {
  companyCode: this.selectedCountryAdd,
  newHolidays: this.holidayForm
 };
 if(this.selectedCountryAdd && this.holidayForm[0].date && this.holidayForm[0].holiday){
  this.spinner.show();
  this.calenderService.addHolidayDetails(newDetails).subscribe(res => {
    if (res['status']) {
     $('#modal-dialog').modal('hide');
     this.spinner.hide();
      this.toastr.success(res['msg']);
      this.holidayForm = [];
      this.calenderDetails(this.selectedYear, this.selectedCountry);
    } else {
      this.spinner.hide();
      this.toastr.error(res['msg']);
    }
  }, err => {
    this.spinner.hide();
    this.toastr.error(err);
  })
 } else{
  this.toastr.error('Enter all details.');
 }
  }

  // update start here
  onEditDateSelect(value) {  // select date from date picker
  }
  // click on update holiday
  updateHoliday(data) {
    this.updateForm = {};
    this.updateForm = data;
    const newdate = data.Date.split("-", 3);
    this.editDate =  {
      "year": Number(newdate[0]),
      "month": Number(newdate[1]),
      "day": Number(newdate[2]),
    };
    this.holidayName = data.Holiday;
  }
  update() {
    const strDate = this.editDate.year + '-' + this.editDate.month + '-'+ this.editDate.day;
    const newDetails = {
      id: this.updateForm['Id'],
      date: strDate,
      holiday: this.holidayName, companyCode: this.updateForm['companyCode'],
    };
    if(this.updateForm['date'] && this.holidayName){
    this.spinner.show();
    this.calenderService.updateHoliday(newDetails).subscribe(res => {
      if (res['status']) {
        $('#modal-dialogedit').modal('hide');
        this.toastr.success(res['msg']);
        this.calenderDetails(this.selectedYear, this.selectedCountry);
      } else {
        this.spinner.hide();
        this.toastr.error(res['msg']);
      }
    }, err => {
      this.spinner.hide();
      this.toastr.error(err);
    })
  } else{
    this.toastr.error('Enter all details.');
  }
  }

  // delete row data
  delete(id, holiday) {
    this.holidayId = {};
    this.holidayId['id'] = id;
    this.holidayName = holiday;
  }
  confirm() {
    $('#exampleModal').modal('hide');
    this.spinner.show();
    this.calenderService.deleteHoliday(this.holidayId).subscribe(res => {
      if (res['status']) {
        this.spinner.hide();
        this.toastr.success(res['msg']);
        this.calenderDetails(this.selectedYear, this.selectedCountry);
      } else {
        this.spinner.hide();
        this.toastr.error(res['msg']);
      }
    }, err => {
      this.spinner.hide();
      this.toastr.error(err);
    })
  }
  decline() {
  }

}
