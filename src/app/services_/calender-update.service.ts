import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CalenderUpdateService {
  
    constructor( private http: HttpClient) {
    }

    getCalenderDetails(year, country) {
        const url = environment.url + `dashboard/get-Calendar-Holiday-By-Year?year=${year}&companyCode=${country}`;
        return this.http.get(url);
    }
     // add holiday details 
     addHolidayDetails(formData) {
        const url = environment.url + 'dashboard/add-Calendar-Holiday-By-Country';
        return this.http.post(url, formData);
    }
    // update pricing 
    updateHoliday(formData) {
        const url = environment.url + 'dashboard/edit-Calendar-Holiday-By-Date';
        return this.http.post(url, formData);
    }
    // delete holiday 
    deleteHoliday(id) {
        const url = environment.url + 'dashboard/delete-Calendar-Holiday-By-Date';
        return this.http.post(url, id);
    }
    //
    companycode() {
        const url = environment.url + `dashboard/get-all-company-code`;
        return this.http.get(url);
    }
}