import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HrDetailsComponent } from './hr-details/hr-details.component';
import { PricingUpdateComponent } from './pricing-update/pricing-update.component';
import { EmployeeMasterDataComponent } from './employee-master-data/employee-master-data.component';
import { SharedServiceComponent } from './shared-service/shared-service.component';
import { PaymentInitiationComponent } from './payment-initiation/payment-initiation.component';
import { ViewEmployeeMasterDataComponent } from './view-employee-master-data/view-employee-master-data.component';
import { Role } from './role';
import { CalenderUpdateComponent } from './calender-update/calender-update.component';
import { UserDashboardComponent } from './user-dashboard/user-dashboard.component';
import { CreatePermissonComponent } from './create-permisson/create-permisson.component';
import { NotificationDetailsComponent } from './notification-details/notification-details.component';
import { AuthguardService } from '../services_/authguard.service';
import { UpdatRPASAPComponent } from './updat-rpa-sap/updat-rpa-sap.component';

const routes: Routes = [
  { path: 'dashboard', component: DashboardComponent, data: { roles: [Role.Admin, Role.Hr] }, canActivate: [AuthguardService], },
  { path: 'employee-master-data', component: EmployeeMasterDataComponent, data: { roles: [Role.Admin, Role.Hr], canActivate: [AuthguardService], } },
  { path: 'notification-details', component: NotificationDetailsComponent, data: { roles: [Role.Admin, Role.Hr, Role.User] }, canActivate: [AuthguardService], },
  { path: 'view-employee-master-data', component: ViewEmployeeMasterDataComponent, data: { roles: [Role.Admin, Role.Hr, Role.User] }, canActivate: [AuthguardService], },
  { path: 'hr-details', component: HrDetailsComponent, data: { roles: [Role.Admin] }, canActivate: [AuthguardService], },
  { path: 'pricing-update', component: PricingUpdateComponent, data: { roles: [Role.Admin] }, canActivate: [AuthguardService], },
  { path: 'payment-workflow', component: SharedServiceComponent, data: { roles: [Role.Admin] }, canActivate: [AuthguardService], },
  { path: 'payment-initiation', component: PaymentInitiationComponent, data: { roles: [Role.Admin] }, canActivate: [AuthguardService], },
  { path: 'calender-update', component: CalenderUpdateComponent, data: { roles: [Role.Admin, Role.Hr] }, canActivate: [AuthguardService], },
  { path: 'create-permission', component: CreatePermissonComponent, data: { roles: [Role.Admin] }, canActivate: [AuthguardService], },
  { path: 'update-leave', component: UpdatRPASAPComponent, data: { roles: [Role.Admin] }, canActivate: [AuthguardService], },
  { path: 'user-dashboard', component: UserDashboardComponent, data: { roles: [Role.Admin, Role.User] }, canActivate: [AuthguardService], },

  { path: '**', redirectTo: '/login', pathMatch: 'full' },
];
// ,Role.User
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ModulesRoutingModule { }
