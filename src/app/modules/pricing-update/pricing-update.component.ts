import { Component, OnInit, TemplateRef } from '@angular/core';
import { PricingService } from 'src/app/services_/pricing.service';
declare var $: any;
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-pricing-update',
  templateUrl: './pricing-update.component.html',
  styleUrls: ['./pricing-update.component.css']
})
export class PricingUpdateComponent implements OnInit {

  pricingData: any;
  addPricing: any = {};
  updatePricing: any = {};
  pricingId: {};
  deleteEmail: any;
  
  constructor(private priceService: PricingService, private toastr: ToastrService) { }

  ngOnInit() {
     this.getPricingData();
  }
  // get all data on page load
  getPricingData() {
    this.priceService.getPricingUpdate().subscribe(res => {
      if(res['status']) {
        this.pricingData = res['data'];
        // this.toastr.success(res['msg']);
      } else{
        this.toastr.error(res['msg']);
     }
    }, err => {
      this.toastr.error(err);
    })
  }

  // call on click of add new pricing
  addNewPricing() {
   this.addPricing = {};
  }
 // add new pricing data
  addPricingData() {
    $('#modal-dialog').modal('hide');
    this.priceService.addPricingUpdate(this.addPricing).subscribe(res => {
      if(res['status']) {
        // this.successMsg = res['msg'];
        this.toastr.success(res['msg']);
        this.getPricingData();
      }else{
        this.toastr.error(res['msg']);
     }
    }, err => {
      this.toastr.error(err);
    })
  }
  // call on edit option
  updatePricingData(data) {
    this.updatePricing = { email: data.MailFrom,
      emailStatus: data.EmailStatus, id: data.Id};
    // this.updatePricing = data;
    // this.updatePricing['id'] = data.Id;
  }
 // update pricing data
  update(){
    const newpricing = { email: this.updatePricing.email,
      emailStatus: this.updatePricing.emailStatus, id: this.updatePricing.id};
    if(newpricing.email && newpricing.emailStatus){
      this.priceService.updatePricingUpdate(newpricing).subscribe(res => {
        if(res['status']) {
          $('#modal-dialogedit').modal('hide');
          this.toastr.success(res['msg']);
          this.getPricingData();
        }else{
          this.toastr.error(res['msg']);
       }
      }, err => {
        this.toastr.error(err);
      })
    } else{
      this.toastr.error('Enter all details.');
    }
  }
 // delete row data
  delete(id, email) {
    this.pricingId = {};
     this.pricingId['id'] = id;
     this.deleteEmail = email;
  }
  confirm(){
    $('#exampleModal').modal('hide');
    this.priceService.deletePricingUpdate(this.pricingId).subscribe(res => {
      if(res['status']) {
        this.toastr.success(res['msg']);
        this.getPricingData();
      }else{
        this.toastr.error(res['msg']);
     }
    }, err => {
      this.toastr.error(err);
    })
  }
  decline(){
  }


}
