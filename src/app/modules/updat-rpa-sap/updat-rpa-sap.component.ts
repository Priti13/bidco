import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { HrDetailsService } from 'src/app/services_/hr-details.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-updat-rpa-sap',
  templateUrl: './updat-rpa-sap.component.html',
  styleUrls: ['./updat-rpa-sap.component.css']
})
export class UpdatRPASAPComponent implements OnInit {
  leaveApplied: any[] = [];
  guidArray: any[] = [];

  constructor(private hrDetailsService: HrDetailsService, private toastr: ToastrService,
    private spinner: NgxSpinnerService) { }

  ngOnInit() {
    this.getLeaveData();
  }
  getLeaveData() {
    this.spinner.show();
    this.hrDetailsService.getUpdatRPASAP().subscribe(res => {
      if (res['status']) {
        this.spinner.hide();
        this.leaveApplied = res['data'];
        this.leaveApplied.map(x => {
          x.botStatus = this.changeBotStatus(x.botStatus);
        })
      } else {
        this.spinner.hide();
        this.toastr.error(res['msg']);
      }
    }, err => {
      this.spinner.hide();
      this.toastr.error(err);
    })
  }

  changeBotStatus(bot) {
    if (bot === 'False') {
      return false;
    }
  }
  replaceLeaveType(value){
    console.log(value.replace(/%/g,' '));
   return value.replace(/%/g,' ');
  }
  // sumit
  onchangeSatus(val) {
    if (this.guidArray.indexOf(val) === -1) {
      this.guidArray.push(val);
    }
  }
  updateLeave() {
    this.spinner.show();
    const newObj = {guidArray: this.guidArray};
    this.hrDetailsService.UpdatRPASAP(newObj).subscribe(res => {
      if (res['status']) {
        this.spinner.hide();
        this.toastr.success('Submitted successfully.');
        this.leaveApplied = [];
        this.getLeaveData();
        this.guidArray =[];
      } else{
        this.spinner.hide();
        this.toastr.error(res['msg']);
      }
    }, err => {
      this.spinner.hide();
      this.toastr.error(err);
    })
  }
}
