import { Component, OnInit } from '@angular/core';
declare var App: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'MLogin';
  urlLink: any;
  navLink: boolean;

  ngOnInit() {
    //  this.loadJSOnInit();
  }

  deactivated() {
    let el2 = document.querySelector('.jsScript2');
    let el3 = document.querySelector('.jsScript3');
    el2.parentNode.removeChild(el2);
    el3.parentNode.removeChild(el3);
    setTimeout(() => {
      this.loadJSOnInit();
    }, 1000);
  }

  async loadJSOnInit() {
    await this.loadScript('assets/plugins/slimscroll/jquery.slimscroll.min.js', 'jsScript2');
    await this.loadScript('assets/js/apps.js', 'jsScript3');
  }

  public loadScript(scriptUrl: string, scriptclass: string) {
    return new Promise((resolve, reject) => {
      const scriptElement = document.createElement('script');
      scriptElement.src = scriptUrl;
      scriptElement.className = scriptclass;
      scriptElement.onload = resolve;
      document.body.appendChild(scriptElement);
    });
  }


  headerShow() {
    const hostlink = window.location.pathname;
    if (localStorage.getItem('currentUser')) {
      const token = JSON.parse(localStorage.getItem('currentUser')).token
      if (hostlink !== '/login' && hostlink !== '/') {
        this.urlLink = true;
        this.navLink = true;
      } else {
        this.urlLink = false;
        this.navLink = false;
      }
    }

  }

}
