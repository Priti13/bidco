import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../services_/authentication.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  returnUrl: string;
  loginForm: any = {};
  constructor(
    private authenticationService: AuthenticationService,
    private route: ActivatedRoute,
    private router: Router, private toastr: ToastrService,
    private spinner: NgxSpinnerService
  ) { }
  // Admin => isneha.yadav@in2ittech.com : 12345
  // Hr => vivekkumar.gupta@in2ittech.com : rvxlc@1#
  ngOnInit() {
    const currentUser = this.authenticationService.currentUserValue;
    if (currentUser === null) {
      this.authenticationService.cleanPreviousUser();
    }
  }
  submitLogin() {
    // wait for response, show spinner..
    this.spinner.show();
    this.authenticationService.userNewLogin(this.loginForm).subscribe(resData => {
      if (resData['status'] === true) {
        const userRole = JSON.parse(localStorage.getItem('currentUser')).role;
        if (userRole === 'super_admin') {
          this.router.navigate(['/modules/dashboard']);
        } else if (userRole === 'hr_user') {
          // this.router.navigate(['/modules/employee-master-data']);
          this.router.navigate(['/modules/dashboard']);
        } else if (userRole === 'user') {
          this.router.navigate(['/modules/user-dashboard']);
        }
      } else {
        this.spinner.hide();
        this.toastr.error(resData['msg']);
      }
    }, () => this.spinner.hide());

  }
  // not is use..
  // async gologin() {
  //   const intrvl = await setInterval(() => {
  //     let userID = localStorage.getItem('userPrincipalName');
  //     let msalToken = localStorage.getItem('msal.idtoken')
  //     if (userID && msalToken) {

  //       this.authenticationService.userLogin({
  //         "email": userID,
  //         "userName": localStorage.getItem('displayName'),
  //         "token": msalToken
  //       }).subscribe(resData => {
  //         if (resData['status'] === true) {
  //           const userRole = JSON.parse(localStorage.getItem('currentUser')).role;
  //           if (userRole === 'super_admin') {
  //             this.router.navigate(['/modules/dashboard']);
  //           } else if (userRole === 'hr_user') {
  //             this.router.navigate(['/modules/employee-master-data']);
  //           } else if (userRole === 'user') {
  //             this.router.navigate(['/modules/user-dashboard']);
  //           }
  //         } else {
  //           this.toastr.error(resData['msg']);
  //         }
  //       });

  //       clearInterval(intrvl);
  //     }
  //   }, 3000);
  // }
}
