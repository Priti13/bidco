import { Component, OnInit } from '@angular/core';
import { EmployeeMasterDataService } from 'src/app/services_/employee-master-data.service';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-notification-details',
  templateUrl: './notification-details.component.html',
  styleUrls: ['./notification-details.component.css']
})
export class NotificationDetailsComponent implements OnInit {

  notificationDetails: any;
  pageIndex: number = 0;
  pageSize = 10;
  // search
  searchForm: any;
  searchBy: any;
  searchByValue: any;
  selectBy = 'Search By';
  totalCount: any;
  constructor(
    private employeeService: EmployeeMasterDataService, private toastr: ToastrService,
    private spinner: NgxSpinnerService, private router: Router) { }

  ngOnInit() {
    this.getNotificationData(this.pageIndex, this.pageSize);
  }
  // get all data on page load
  getNotificationData(pageIndex, pageSize) {
    this.spinner.show();
    this.notificationDetails = [];
    this.totalCount = 0;
    this.employeeService.getNotificationData(pageSize * pageIndex, pageSize).subscribe(res => {
      if (res['status']) {
        this.spinner.hide();
        this.notificationDetails = res['data'];
        this.totalCount = res['total'];
      } else {
        this.spinner.hide();
        this.toastr.error(res['msg']);
      }
    }, err => {
      this.spinner.hide();
      this.toastr.error(err);
    });
  }
  // pagination
  pageChanged(event) {
    // update current page of items
    this.pageIndex = event;
    this.getNotificationData(this.pageIndex, this.pageSize);
  }
  redirect(empdata: any) {
    this.employeeService.setData(empdata);
    this.router.navigateByUrl('/modules/view-employee-master-data');
  }

}
