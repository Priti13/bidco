import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CreatePermissionService {
  
    constructor( private http: HttpClient) {
    }

    getcreatePermission() {
        const url = environment.url + 'data-tables/get-permission-list';
        return this.http.get(url);
    }
 // add pricing 
    addPermission(formData) {
        const url = environment.url + 'data-tables/add-user-permission';
        return this.http.post(url, formData);
    }
    // update pricing 
    updatePermission(formData) {
        const url = environment.url + 'data-tables/update-price-update-list';
        return this.http.post(url, formData);
    }
    // delete pricing 
    deletePermission(id) {
        const url = environment.url + 'data-tables/delete-permission';
        return this.http.post(url, id);
    }
    // reset
    resetPermission(formData) {
        const url = environment.url + 'data-tables/reset-permission-password';
        return this.http.post(url, formData);
    }
}