import { Role } from "./role";

export class User {
    id?: number;
    user?: string;
    email?: string;
    username?: string;
    password?: string;
    firstName?: string;
    lastName?: string;
    role: Role;
    status: boolean;
    token?: string;

}


