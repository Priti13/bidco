import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PricingService {
  
    constructor( private http: HttpClient) {
    }

    getPricingUpdate() {
        const url = environment.url + 'data-tables/price-update-list';
        return this.http.get(url);
    }
 // add pricing 
    addPricingUpdate(formData) {
        const url = environment.url + 'data-tables/price-update-list';
        return this.http.post(url, formData);
    }
    // update pricing 
    updatePricingUpdate(formData) {
        const url = environment.url + 'data-tables/update-price-update-list';
        return this.http.post(url, formData);
    }
    // delete pricing 
    deletePricingUpdate(id) {
        const url = environment.url + 'data-tables/remove-price-update-list';
        return this.http.post(url, id);
    }
}