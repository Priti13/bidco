// import { Component, OnInit } from '@angular/core';
import { group, animate, query, style, trigger, transition, state } from '@angular/animations';
import { Component, Input, Output, EventEmitter, ElementRef, HostListener, ViewChild, OnInit, AfterViewChecked } from '@angular/core';
import { AuthenticationService } from 'src/app/services_/authentication.service';
import { Router } from '@angular/router';
declare var $: any;
import { User } from './../user';
import { Role } from './../role';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css'],
  animations: [
    trigger('expandCollapse', [
      state('expand', style({ height: '*', overflow: 'hidden', display: 'block' })),
      state('collapse', style({ height: '0px', overflow: 'hidden', display: 'none' })),
      state('active', style({ height: '*', overflow: 'hidden', display: 'block' })),
      transition('expand <=> collapse', animate(100)),
      transition('active => collapse', animate(100))
    ])
  ]
})
export class MenuComponent implements OnInit {
  name: string;
  email: string;
  currentUser: User;
  constructor(
    private router: Router,
    private authenticationService: AuthenticationService
  ) {

    
  }
  ngOnInit() {
    this.authenticationService.currentUser.subscribe(x => this.currentUser = x);
    // tslint:disable-next-line: only-arrow-functions
    $("#main-menu").click(function() {
      $("#sub-nav").toggle();
    });

    this.name = this.currentUser.user;
    this.email = this.currentUser.email;
  }
  get isHr() {
    return this.currentUser && this.currentUser.role === Role.Hr;
  }
  get isAdmin() {
    return this.currentUser && this.currentUser.role === Role.Admin;
  }
}
