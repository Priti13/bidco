import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdatRPASAPComponent } from './updat-rpa-sap.component';

describe('UpdatRPASAPComponent', () => {
  let component: UpdatRPASAPComponent;
  let fixture: ComponentFixture<UpdatRPASAPComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdatRPASAPComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdatRPASAPComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
