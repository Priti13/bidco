import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeMasterDataComponent } from './employee-master-data.component';

describe('EmployeeMasterDataComponent', () => {
  let component: EmployeeMasterDataComponent;
  let fixture: ComponentFixture<EmployeeMasterDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeeMasterDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeMasterDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
