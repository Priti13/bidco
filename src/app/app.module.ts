import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { FormsModule } from '@angular/forms';
import { NgxSpinnerModule } from 'ngx-spinner';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { AuthguardService } from './services_/authguard.service';
import { AuthenticationService } from './services_/authentication.service';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ModulesModule } from './modules/modules.module';
import { JwtInterceptor } from './services_/auth.interceptor';
import { ErrorInterceptor } from './services_/error.interceptor';
// import { MenuComponent } from './modules/menu/menu.component';
// import { NavComponent } from './modules/nav/nav.component';



@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    // MenuComponent,
    // NavComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ModulesModule,
    BrowserAnimationsModule,
    FormsModule,
    ToastrModule.forRoot(),
    NgxSpinnerModule,
  ],
  providers: [
    AuthguardService,
    AuthenticationService,
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
