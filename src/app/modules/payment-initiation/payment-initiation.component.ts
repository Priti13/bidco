import { Component, OnInit } from '@angular/core';
import { SharedService } from 'src/app/services_/shared-service.service';
declare var $: any;
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-payment-initiation',
  templateUrl: './payment-initiation.component.html',
  styleUrls: ['./payment-initiation.component.css']
})
export class PaymentInitiationComponent implements OnInit {

  paymentInitiation: any;
  addPaymentIniti: any = {};
  updatePaymentIniti: any = {};
  deleteEmail: any;
  paymentiniti: {};
  constructor(private service: SharedService, private toastr: ToastrService) { }

  ngOnInit() {
    this.getpaymentInitiation();
  }

  // get all data on page load
  getpaymentInitiation() {
    this.service.getPaymentData().subscribe(res => {
      if (res['status']) {
        this.paymentInitiation = res['data'];
      } else{
         this.toastr.error(res['msg']);
      }
    }, err => {
      this.toastr.error(err);
    })
  }
  // call on click of add new paymentiniti....
  addNewPayment() {
    this.addPaymentIniti = {};
  }
  // add new paymentiniti data
  addPaymentInitiData() {
    $('#modal-dialog').modal('hide');
    this.service.addPaymentIniti(this.addPaymentIniti).subscribe(res => {
      if (res['status']) {
        this.toastr.success(res['msg']);
        this.getpaymentInitiation();
      }else{
        this.toastr.error(res['msg']);
     }
    }, err => {
      this.toastr.error(err);
    })
  }

  // call on edit option
  updatePaymentInitiData(data) {
    this.updatePaymentIniti = data;
    this.updatePaymentIniti['id'] = data.Id;
  }
  // update pricing data
  update() {
    $('#modal-dialogedit').modal('hide');
    const newpayment = { email: this.updatePaymentIniti.MailFrom,
      emailStatus: this.updatePaymentIniti.EmailStatus, id: this.updatePaymentIniti.id};
    this.service.updatePaymentIniti(newpayment).subscribe(res => {
      if (res['status']) {
        this.toastr.success(res['msg']);
        this.getpaymentInitiation();
      }else{
        this.toastr.error(res['msg']);
     }
    }, err => {
      this.toastr.error(err);
    })
  }

 // delete row data
 delete(id, email) {
  this.paymentiniti = {};
  this.paymentiniti['id'] = id;
  this.deleteEmail = email;
}
  confirm(){
    $('#exampleModal').modal('hide');
    this.service.deletePaymentIniti(this.paymentiniti).subscribe(res => {
      if(res['status']) {
        this.toastr.success(res['msg']);
        this.getpaymentInitiation();
      }else{
        this.toastr.error(res['msg']);
     }
    }, err => {
      this.toastr.error(err);
    })
  }
  decline(){
  }

}
