import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SharedService {
  
    constructor( private http: HttpClient) {
    }

    getPaymentData() {
        const url = environment.url + 'data-tables/payment-init-list';
        return this.http.get(url);
    }
 // add pricing 
    addPaymentIniti(formData) {
        const url = environment.url + 'data-tables/payment-init-list';
        return this.http.post(url, formData);
    }
    // update pricing 
    updatePaymentIniti(formData) {
        const url = environment.url + 'data-tables/update-payment-init-list';
        return this.http.post(url, formData);
    }
    // delete pricing 
    deletePaymentIniti(id) {
        const url = environment.url + 'data-tables/remove-payment-init-list';
        return this.http.post(url, id);
    }

    // services for payment work flow

    getPaymentWorkflow() {
        const url = environment.url + 'data-tables/payment-work-list';
        return this.http.get(url);
    }
 // add pricing 
    addPaymentWorkflow(formData) {
        const url = environment.url + 'data-tables/payment-work-list';
        return this.http.post(url, formData);
    }
    // update pricing 
    updatePaymentWorkflow(formData) {
        const url = environment.url + 'data-tables/update-payment-work-list';
        return this.http.post(url, formData);
    }
    // delete pricing 
    deletePaymentWorkflow(id) {
        const url = environment.url + 'data-tables/remove-payment-work-list';
        return this.http.post(url, id);
    }
}