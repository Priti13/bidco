import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewEmployeeMasterDataComponent } from './view-employee-master-data.component';

describe('ViewEmployeeMasterDataComponent', () => {
  let component: ViewEmployeeMasterDataComponent;
  let fixture: ComponentFixture<ViewEmployeeMasterDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewEmployeeMasterDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewEmployeeMasterDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
