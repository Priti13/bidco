import { Component, OnInit } from '@angular/core';
import { SharedService } from 'src/app/services_/shared-service.service';
declare var $: any;
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-shared-service',
  templateUrl: './shared-service.component.html',
  styleUrls: ['./shared-service.component.css']
})
export class SharedServiceComponent implements OnInit {

  paymentWorkflowData: any;
  addPaymentworkflow: any = {};
  updatePaymentworkflow: any = {};
  paymentwrkflow: {};
  deleteEmail: any;
  constructor(private service: SharedService, private toastr: ToastrService) { }

  ngOnInit() {
    this.getpaymentWorkFlow();
  }
   // get all data on page load
   getpaymentWorkFlow() {
    this.service.getPaymentWorkflow().subscribe(res => {
      if (res['status']) {
        this.paymentWorkflowData = res['data'];
        // this.toastr.success(res['msg']);
      }else{
        this.toastr.error(res['msg']);
     }
    }, err => {
      this.toastr.error(err);
    })
  }

    // call on click of add new paymentiniti....
    addNewPaymentworkflow() {
      this.addPaymentworkflow = {};
    }
    // add new paymentiniti data
    addPaymentInitiData() {
      this.service.addPaymentWorkflow(this.addPaymentworkflow).subscribe(res => {
        if (res['status']) {
          $('#modal-dialog').modal('hide');
          this.toastr.success(res['msg']);
          this.getpaymentWorkFlow();
        }else{
          this.toastr.error(res['msg']);
       }
      }, err => {
        this.toastr.error(err);
      })
    }
  
    // call on edit option
    updatePaymentWorkflowData(data) {
      this.updatePaymentworkflow = { email: data.MailFrom,
        emailStatus: data.EmailStatus, id: data.Id};
      // this.updatePaymentworkflow = data;
      // this.updatePaymentworkflow['id'] = data.Id;
    }
  //  update pricing data
    update() {
      const newupdateworkflow = { email: this.updatePaymentworkflow.email,
         emailStatus: this.updatePaymentworkflow.emailStatus, id: this.updatePaymentworkflow.id};
      this.service.updatePaymentWorkflow(newupdateworkflow).subscribe(res => {
        if (res['status']) {
          $('#modal-dialogedit').modal('hide');
          this.toastr.success(res['msg']);
          this.getpaymentWorkFlow();
        }else{
          this.toastr.error(res['msg']);
       }
      }, err => {
        this.toastr.error(err);
      })
    }
  
   // delete row data
   delete(id, email) {
    this.paymentwrkflow = {};
     this.paymentwrkflow['id'] = id;
     this.deleteEmail = email;
  }
  confirm(){
    $('#exampleModal').modal('hide');
    this.service.deletePaymentWorkflow(this.paymentwrkflow).subscribe(res => {
      if(res['status']) {
        this.toastr.success(res['msg']);
        this.getpaymentWorkFlow();
      }else{
        this.toastr.error(res['msg']);
     }
    }, err => {
      this.toastr.error(err);
    })
  }
  decline(){
  }

}
