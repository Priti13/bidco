import { Component, OnInit } from '@angular/core';
import { EmployeeMasterDataService } from 'src/app/services_/employee-master-data.service';
import { ToastrService } from 'ngx-toastr';
import { HrDetailsService } from 'src/app/services_/hr-details.service';
declare var $: any;
import * as moment from 'moment';

@Component({
  selector: 'app-view-employee-master-data',
  templateUrl: './view-employee-master-data.component.html',
  styleUrls: ['./view-employee-master-data.component.css']
})
export class ViewEmployeeMasterDataComponent implements OnInit {
  masterData: any;
  update_form: any;

  firstMangerName: string;
  secondManagerName: string;
  substitutePersNoName: string;
  // moment = require('moment');
  newBirthDate = '';
  searchForm ={};
  // tslint:disable-next-line: max-line-length
  constructor(private employeeService: EmployeeMasterDataService, private toastr: ToastrService,
    private hrDetailsService: HrDetailsService) { }
  ngOnInit() {
    this.update_form = {
      Mobile: '',
      substitutePersNo: '',
      second_Status: '',
      Id: 0,
      Higher_Manager_Status: '',
      first_Manager_Pers_No: '',
      second_manager_no: ''
    }
    // this.masterData = history.state.data;
    this.masterData = this.employeeService.getData;
    // birth date
    if (this.masterData.Birth_Date !== undefined && this.masterData.Birth_Date !== '') {
      this.masterData.Birth_Date = moment(this.masterData.Birth_Date, "YYYYMMDD").calendar();
    }

    // date of joining date
    if (this.masterData.DOJ !== undefined && this.masterData.DOJ !== '') {
      this.masterData.DOJ = moment(this.masterData.DOJ, "YYYYMMDD").calendar();
    }

    // date of leaving date
    if (this.masterData.Leaving !== undefined && this.masterData.Leaving !== '') {
      this.masterData.Leaving = moment(this.masterData.Leaving, "YYYYMMDD").calendar();
    }
  this.formatRowData();
  }

  formatRowData(){
    const first_Manager_Pers_No = this.masterData['1st_Manager_Pers_No'];
    this.masterData['first_Manager_Pers_No'] = first_Manager_Pers_No;
    const second_Status = this.masterData['2B_Status'];
    this.masterData['second_Status'] = second_Status;
  }

  editForm() {
    this.update_form = this.masterData;
    this.formatRowData();
  }
  // first manager name
  getfirstManagerName(id) {
    this.hrDetailsService.getEmployeeDetails(id.value).subscribe(res => {
      if (res['status'] && res['data'].length != 0) {
        const details = res['data'];
        this.firstMangerName = details[0].First_Name + ' ' + details[0].Last_Name;
      }else{
        this.toastr.error(res['msg']);
     }
    }, err => {
      this.toastr.error(err);
    })
  }

  // first manager name
  getSecondManagerName(id) {
    this.hrDetailsService.getEmployeeDetails(id.value).subscribe(res => {
      if (res['status'] && res['data'].length != 0) {
        const details = res['data'];
        this.secondManagerName = details[0].First_Name + ' ' + details[0].Last_Name;
      } else{
        this.toastr.error(res['msg']);
     }
    }, err => {
      this.toastr.error(err);
    })
  }

  // 
  getsubstitutePersNoName(id) {
    this.hrDetailsService.getEmployeeDetails(id.value).subscribe(res => {
      if (res['status'] && res['data'].length != 0) {
        const details = res['data'];
        this.substitutePersNoName = details[0].First_Name + ' ' + details[0].Last_Name;
      } else{
        this.toastr.error(res['msg']);
     }
    }, err => {
      this.toastr.error(err);
    })
  }

  // update pricing data
  update() {
    this.update_form['FirstManager'] = this.firstMangerName;
    this.update_form['SecondManager'] = this.secondManagerName;
    $('#modal-dialog').modal('hide');
    const newpayment = {
      mobile: this.update_form.Mobile,
      secondBStatus: this.update_form.second_Status,
      substitutePersNo: this.update_form.substitutePersNo,
      persNo: this.update_form.Pers_No,
      higherManagerStatus: this.update_form.Higher_Manager_Status,
      firstManagerPersonId: this.update_form.first_Manager_Pers_No,
      secondMangerPersonId: this.update_form.second_manager_no
    };
    this.employeeService.updateEmployeeMasterDetails(newpayment).subscribe(res => {
      if (res['status']) {
        this.toastr.success(res['msg']);
        this.updateDetails();
      } else {
        this.toastr.error(res['msg']);
      }
    }, err => {
      this.toastr.error(err);
    })
  }

 updateDetails(){
  this.searchForm['personNo'] = this.masterData.Pers_No;
  this.employeeService.searchDetails(this.searchForm).subscribe(res => {
    this.masterData = res['data'][0];
  }, err => {
    this.toastr.error(err);
  })
 }

}
