import { Injectable } from '@angular/core';
import { CanActivate, Router, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
import { AuthenticationService } from './authentication.service';

@Injectable({
  providedIn: 'root'
})
export class AuthguardService implements CanActivate {
  constructor(private router: Router, private authenticationService: AuthenticationService) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    // var currentUser = localStorage.getItem('currentUser');
    const currentUser = this.authenticationService.currentUserValue;
    if (currentUser && currentUser.token != undefined) {
      // check if route is restricted by role
      if (route.data.roles && route.data.roles.indexOf(currentUser.role) === -1) {
        // role not authorised so redirect to home page respected to current user
        if (currentUser.role === 'super_admin') {
          this.router.navigate(['/modules/dashboard']);
        } else if (currentUser.role === 'hr_user') {
          this.router.navigate(['/modules/dashboard']);
        } else if (currentUser.role === 'user') {
          this.router.navigate(['/modules/user-dashboard']);
        }
        return false;
      }
      // authorised so return true
      return true;
    }
    // not logged in so redirect to login page with the return url
    this.router.navigate(['/login'], { queryParams: { returnUrl: state.url } });
    return false;
  }
}
