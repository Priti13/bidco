import { Component, OnInit } from '@angular/core';
import { HrDetailsService } from 'src/app/services_/hr-details.service';
declare var $: any;
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-hr-details',
  templateUrl: './hr-details.component.html',
  styleUrls: ['./hr-details.component.css']
})
export class HrDetailsComponent implements OnInit {

  hrDetailsData: any;
  employeeDetails: any = [];
  addHrDetails: any = {};
  email: any;
  name: any;
  cc_name: any;
  cc_email: any;
  updateDetails_hr: any = {};
  hr_ID: {};
  deleteEmail: any;
  constructor(private hrDetailsService: HrDetailsService, private toastr: ToastrService,
    private spinner: NgxSpinnerService) { }

  ngOnInit() {
    this.getHrDetailsData();
  }
  // get all data on page load
  getHrDetailsData() {
    this.spinner.show();
    this.hrDetailsService.getHrDetails().subscribe(res => {
      if (res['status']) {
        this.spinner.hide();
        this.hrDetailsData = res['data'];
      } else{
        this.spinner.hide();
      this.toastr.error(res['msg']);
      }
    }, err => {
      this.spinner.hide();
      this.toastr.error(err);
    })
  }
  // get all Employee Details 
  getEmployeeDetailsData(id, user) {
    this.hrDetailsService.getEmployeeDetails(id).subscribe(res => {
      if (res['status']) {
        this.employeeDetails = res['data'];
        if (this.employeeDetails.length !== 0) {

          if (user === 'personNo') {
            this.email = this.employeeDetails[0].Email;
            this.name = this.employeeDetails[0].First_Name + this.employeeDetails[0].Last_Name;
          }
          if (user === 'personNoSecond') {
            this.cc_email = this.employeeDetails[0].Email;
            this.cc_name = this.employeeDetails[0].First_Name + this.employeeDetails[0].Last_Name;
          }
          if (user === 'personNoEdit') {
            this.updateDetails_hr.First_Name = this.employeeDetails[0].First_Name;
            this.updateDetails_hr.Last_Name = this.employeeDetails[0].Last_Name;
            this.updateDetails_hr.Email = this.employeeDetails[0].Email;
          }
          if (user === 'personNoSecondEdit') {
            this.updateDetails_hr.cc_email = this.employeeDetails[0].Email;
            this.updateDetails_hr.cc_first_name = this.employeeDetails[0].First_Name;
            this.updateDetails_hr.cc_last_name = this.employeeDetails[0].Last_Name;
          }
        } else if(this.employeeDetails.length === 0 && user === 'personNo'){
          this.name = '';
          this.email = '';
        } else if(this.employeeDetails.length === 0 && user === 'personNoSecond'){
          this.cc_email = '';
          this.cc_name = '';
        } else if(this.employeeDetails.length === 0 && user === 'personNoEdit') {
          this.updateDetails_hr.First_Name = '';
          this.updateDetails_hr.Last_Name = '';
          this.updateDetails_hr.Email = '';
        } else if(this.employeeDetails.length === 0 && user === 'personNoSecondEdit') {
          this.updateDetails_hr.cc_email = '';
          this.updateDetails_hr.cc_first_name = '';
          this.updateDetails_hr.cc_last_name = '';
        }
      } else{
        this.toastr.error(res['msg']);
      }
    }, err => {
      this.toastr.error(err);
    })
  }
  // call on click of add new add

  addNewHrDetails() {
    this.addHrDetails = {};
    this.addHrDetails.pArea = 'Select Option'
    this.name = '';
    this.email = '';
    this.cc_email = '';
    this.cc_name = '';
  }
  // after entering person id
  getEmployee(id, user) {
    this.getEmployeeDetailsData(id.value, user);
  }
  // after entering person_cc id
  getEmployee_cc(id, user) {
    this.getEmployeeDetailsData(id.value, user);
  }
  // add new pricing data
  addHrDetailsData() {
    if(this.addHrDetails.pArea !== 'Select Option' && this.addHrDetails){
      this.spinner.show();
      this.hrDetailsService.addPricingUpdate(this.addHrDetails).subscribe(res => {
        if (res['status']) {
          this.spinner.hide();
          $('#modal-dialog').modal('hide');
          this.toastr.success(res['msg']);
          this.getHrDetailsData();
        } else{
          this.spinner.hide();
          this.toastr.error(res['msg']);
        }
      }, err => {
        this.spinner.hide();
        this.toastr.error(err);
      })
    } else{
      this.toastr.error('Enter all details.');
    }
  }
  // call on edit option
  updateHrDetailsData(data) {
    this.updateDetails_hr = {};
    this.updateDetails_hr = data;
    this.updateDetails_hr.personNo = data.Pers_No;
    this.updateDetails_hr.personNoSecond = data.Pers_No_Second;
    this.updateDetails_hr.pArea = data.PArea;
    this.updateDetails_hr.hrId = data.hr_id;
    this.updateDetails_hr = data;
    // this.updateDetails_hr['hrId'] = data.hr_id;
  }
  // update pricing data
  update() {
    const newDetails = {
      personNo: this.updateDetails_hr.personNo,
      personNoSecond: this.updateDetails_hr.personNoSecond, pArea: this.updateDetails_hr.pArea, hrId: this.updateDetails_hr.hrId
    };
    if(newDetails.pArea && newDetails.personNo && newDetails.personNoSecond){
    this.spinner.show();
    this.hrDetailsService.updateHr_details(newDetails).subscribe(res => {
      if (res['status']) {
        $('#modal-dialogedit').modal('hide');
        this.toastr.success(res['msg']);
        this.getHrDetailsData();
      } else{
        this.spinner.hide();
        this.toastr.error(res['msg']);
      }
    }, err => {
      this.spinner.hide();
      this.toastr.error(err);
    })
  } else{
    this.toastr.error('Enter all details.');
  }
  }
  // delete row data
  delete(id, email) {
    this.hr_ID = {};
    this.hr_ID['hrId'] = id;
    this.deleteEmail = email;
  }
  confirm() {
    $('#exampleModal').modal('hide');
    this.spinner.show();
    this.hrDetailsService.deleteHrDetails(this.hr_ID).subscribe(res => {
      if (res['status']) {
        this.spinner.hide();
        this.toastr.success(res['msg']);
        this.getHrDetailsData();
      } else{
        this.spinner.hide();
        this.toastr.error(res['msg']);
      }
    }, err => {
      this.spinner.hide();
      this.toastr.error(err);
    })
  }
  decline() {
  }


}
