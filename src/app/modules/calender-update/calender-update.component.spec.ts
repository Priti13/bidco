import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalenderUpdateComponent } from './calender-update.component';

describe('CalenderUpdateComponent', () => {
  let component: CalenderUpdateComponent;
  let fixture: ComponentFixture<CalenderUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CalenderUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalenderUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
