import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class HrDetailsService {
  
    constructor( private http: HttpClient) {
    }

    getHrDetails() {
        const url = environment.url + 'data-tables/hr-details';
        return this.http.get(url);
    }
    getEmployeeDetails(id) {
        const url = environment.url + `data-tables/employee-details?id=${id}`;
        return this.http.get(url);
    }
     // add hr details 
     addPricingUpdate(formData) {
        const url = environment.url + 'data-tables/add-hr-details';
        return this.http.post(url, formData);
    }
    // update pricing 
    updateHr_details(formData) {
        const url = environment.url + 'data-tables/update-hr-details';
        return this.http.post(url, formData);
    }
    // delete pricing 
    deleteHrDetails(id) {
        const url = environment.url + 'data-tables/remove-hr-details';
        return this.http.post(url, id);
    }

    // update 
    getUpdatRPASAP() {
        const url = environment.url + 'data-tables/leave-applied';
        return this.http.get(url);
    }
    UpdatRPASAP(obj) {
        const url = environment.url + 'data-tables/update-leave-applied';
        return this.http.post(url, obj);
    }
}