import { Component, OnInit } from '@angular/core';
import { EmployeeMasterDataService } from 'src/app/services_/employee-master-data.service';
declare var $: any;
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from '@angular/router';


@Component({
  selector: 'app-employee-master-data',
  templateUrl: './employee-master-data.component.html',
  styleUrls: ['./employee-master-data.component.css']
})
export class EmployeeMasterDataComponent implements OnInit {

  employeeMasterDetails: any;
  pageIndex: number = 1;
  pageSize = 10;
  // search
  searchForm: any;
  searchBy: any;
  searchByValue: any;
  selectBy = 'Search By';
  totalCount: any;
  showPagination: boolean;
  constructor(
    private employeeService: EmployeeMasterDataService, private toastr: ToastrService,
    private spinner: NgxSpinnerService, private router: Router) { }

  ngOnInit() {
    // this.showPagination = true;
    this.getEmployeeMasterData(this.pageIndex, this.pageSize);
  }
  // get all data on page load
  getEmployeeMasterData(pageIndex, pageSize) {
    this.showPagination = true;
    this.pageSize = 10;
    this.spinner.show();
    this.employeeMasterDetails = [];
    this.totalCount = 0;
    this.employeeService.getEmployeeMasterDetails(pageSize*(pageIndex-1), pageSize).subscribe(res => {
      if (res['status']) {
        this.spinner.hide();
        this.employeeMasterDetails = res['data'];
        this.totalCount = res['total'];
      } else {
        this.spinner.hide();
        this.toastr.error(res['msg']);
      }
    }, err => {
      this.spinner.hide();
      this.toastr.error(err);
    });
  }
  // pagination
  pageChanged(event) {
    // update current page of items
    this.pageIndex = event;
    this.getEmployeeMasterData(this.pageIndex, this.pageSize);
  }

  // get data from api to convert into excel
  exportToExcel() {
    this.employeeService.getExcelDetails().subscribe(res => {
      this.downLoadFile(res, "application/ms-excel");
    }, err => {
      this.toastr.error(err);
    })
  }

  downLoadFile(data: any, type: string) {
    let dataType = data.type;
    let binaryData = [];
    binaryData.push(data);
    let downloadLink = document.createElement('a');
    downloadLink.href = window.URL.createObjectURL(new Blob(binaryData, { type: dataType }));
    downloadLink.setAttribute('download', 'Employee Details');
    document.body.appendChild(downloadLink);
    downloadLink.click();
  }
  // search in table
  searchByDropdown(selectedValue) {
    this.searchForm = {};
    this.searchByValue = selectedValue;
  }
  searchTable(searchValue) {

    if (this.searchByValue === 'personNo') {
      this.searchForm['personNo'] = searchValue.value;
    } else if (this.searchByValue === 'email') {
      this.searchForm['email'] = searchValue.value;
    } else if (this.searchByValue === 'firstName') {
      this.searchForm['firstName'] = searchValue.value;
    } else if (this.searchByValue === 'lastName') {
      this.searchForm['lastName'] = searchValue.value;
    }
    if(this.searchForm && searchValue.value){
      this.spinner.show();
      this.showPagination = false;
      this.employeeMasterDetails = [];
      this.employeeService.searchDetails(this.searchForm).subscribe(res => {
        if (res['status']) {
          this.spinner.hide();
          this.employeeMasterDetails = res['data'];
        } else {
          this.spinner.hide();
          this.toastr.error(res['msg']);
        }
      }, err => {
        this.spinner.hide();
        this.toastr.error(err);
      })
    } else{
      this.toastr.error('Select any search value.');
    }
 
  }

  redirect(empdata: any) {
    this.employeeService.setData(empdata);
    this.router.navigateByUrl('/modules/view-employee-master-data');
  }
}
